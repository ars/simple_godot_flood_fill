extends TileMap


var used_cells = []
var fill_color = 2
var default_tilemap_index = []
var slow_fill = false


func _ready():
	used_cells = self.get_used_cells()

	for u in used_cells:
		default_tilemap_index.append(get_cellv(u))


func _input(event):
	if event is InputEventMouseButton:
		if Input.is_action_just_pressed("left_click"):
			var mouse_pos = world_to_map(get_global_mouse_position())
			
			if used_cells.has(mouse_pos) && get_cellv(mouse_pos) != fill_color:
				_flood_fill(mouse_pos)


func _flood_fill(mouse_pos):
	var start_pos = mouse_pos
	var neighbors = [Vector2.RIGHT, Vector2.LEFT, Vector2.UP, Vector2.DOWN]
	var checked = []
	var queue = [start_pos]

	set_cellv(start_pos, fill_color)

	while queue.empty() == false:
		var current = queue.pop_back()
		checked.append(current)

		for n in neighbors:
			var next_tile = current + n

			if used_cells.has(next_tile) == false:
				continue
			if checked.has(next_tile):
				continue
			if get_cellv(next_tile) == fill_color:
				continue
			if slow_fill == true:
				yield(get_tree().create_timer(0.07), "timeout")

			queue.append(next_tile)
			set_cellv(next_tile, fill_color)


func _reset_tiles():
	var entry = 0

	for u in used_cells:
		set_cellv(u, default_tilemap_index[entry])
		entry += 1


func _on_reset_tiles_button_pressed():
	_reset_tiles()


func _on_slow_fill_toggle_toggled(button_pressed):
	slow_fill = button_pressed
