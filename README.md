# Simple Godot Flood Fill

<img src="https://codeberg.org/ars/simple_godot_flood_fill/raw/branch/main/example.gif" width="512" height="300" />

A simple implementation of the [flood fill algorithm](https://en.wikipedia.org/wiki/Flood_fill) for Godot writtin in GDScript. I made this because I struggled to wrap my head around all the other flood fill examples I saw. I'm still relatively new to programming so I doubt this is as performant or "correct" as it could be. Any improvement suggestions are welcome! Though please keep in mind the intention of this isn't to be the most performant flood fill algorithm ever. Rather, it's designed to be easy to understand and extend.

## How it works
### Variables:
First we [inherit](https://docs.godotengine.org/en/stable/tutorials/scripting/gdscript/gdscript_basics.html#inheritance) the [TileMap](https://docs.godotengine.org/en/stable/classes/class_tilemap.html) global class so we can use the [get_used_cells()](https://docs.godotengine.org/en/stable/classes/class_tilemap.html#class-tilemap-method-get-used-cells), [get_cellv()](https://docs.godotengine.org/en/stable/classes/class_tilemap.html#class-tilemap-method-get-cellv), [set_cellv()](https://docs.godotengine.org/en/stable/classes/class_tilemap.html#class-tilemap-method-set-cellv), and [world_to_map()](https://docs.godotengine.org/en/stable/classes/class_tilemap.html#class-tilemap-method-world-to-map) methods.

    extends TileMap

Next we create the used_cells variable and set it to an empty [array](https://docs.godotengine.org/en/stable/classes/class_array.html). We'll fill this array with all the used cells within the [TileMap](https://docs.godotengine.org/en/stable/classes/class_tilemap.html) later.

    var used_cells = []

We set the fill_color variable to the desired tile index (in our case green, which in our tileset index is 2.) Keep in mind the index starts counting at 0 not 1, so the index number for 32x32_tileset_3.png (green) would be 2 even though it's the third tile.

    var fill_color = 2
 

Then we create an empty [array](https://docs.godotengine.org/en/stable/classes/class_array.html) for storing the default cell index for each used cell so we can reset the cells later.
 
    var default_tilemap_index = []

We create a variable called slow_fill to tell the flood fill algorithm whether to fill slowly or not.

    var slow_fill = false

### Ready function

When our TileMap node is [ready](https://docs.godotengine.org/en/stable/classes/class_node.html#class-node-method-ready), we call [get_used_cells()](https://docs.godotengine.org/en/stable/classes/class_tilemap.html#class-tilemap-method-get-used-cells) on it, which returns an [array](https://docs.godotengine.org/en/stable/classes/class_array.html) of coordinates for every used cell on our TileMap. We store that array in the used_cells variable. Then we use a [for](https://docs.godotengine.org/en/stable/tutorials/scripting/gdscript/gdscript_basics.html#for) loop to get the default tile index for every cell in used_cells.

    func _ready():
	    used_cells = self.get_used_cells()

	    for u in used_cells:
		    default_tilemap_index.append(get_cellv(u))

### Handling input from the user:

When an input occurs, we check if the input is a mouse button and if that mouse button is left click. Using the [world_to_map()](https://docs.godotengine.org/en/stable/classes/class_tilemap.html#class-tilemap-method-world-to-map) method, we get the coordinates of where the mouse was on the TileMap when left click was pressed and store it in the mouse_pos variable. If used_cells has the mouse coordinates and the cell's index is not fill_color, we call _flood_fill and pass it mouse_pos.  

    func _input(event):
    	if event is InputEventMouseButton:
    		if Input.is_action_just_pressed("left_click"):
    			var mouse_pos = world_to_map(get_global_mouse_position())
    			
    			if used_cells.has(mouse_pos) && get_cellv(mouse_pos) != fill_color:
    				_flood_fill(mouse_pos)

### The flood fill algorithm:
We set the start position variable (start_pos) to the mouse position (mouse_pos.) Then we define the directions of each neighbor using [Vector2](https://docs.godotengine.org/en/stable/classes/class_vector2.html)s. We create an empty [array](https://docs.godotengine.org/en/stable/classes/class_array.html) called checked which we will use to keep track of which cells the flood fill algorithm has already checked. Next we create an [array](https://docs.godotengine.org/en/stable/classes/class_array.html) for our queue and store our start position in it.

    func _flood_fill(mouse_pos):
	    var start_pos = mouse_pos
	    var neighbors = [Vector2.RIGHT, Vector2.LEFT, Vector2.UP, Vector2.DOWN]
	    var checked = []
	    var queue = [start_pos]

We set the index number of start_pos to the fill color using [set_cellv()](https://docs.godotengine.org/en/stable/classes/class_tilemap.html#class-tilemap-method-set-cellv). This is so when the flood fill algorithm starts the start position will also get filled.

	    set_cellv(start_pos, fill_color)

This is the main loop. While the queue is not empty, we set the variable 'current' to the entry at the back of the queue using the [pop_back()](https://docs.godotengine.org/en/stable/classes/class_array.html#class-array-method-pop-back) method. We then add current to checked because it's being checked.

    	while queue.empty() == false:
    		var current = queue.pop_back()
    		checked.append(current)

For every entry in neighbors we set the variable 'next_tile' to be the 'current' variable + the entry value to get the neighboring tile. If the neighboring tile is not in the TileMap, we [continue](https://docs.godotengine.org/en/stable/tutorials/scripting/gdscript/gdscript_basics.html#keywords). Continuing will immediately skip to the next iteration of the for loop. We then see if next_tile has already been checked and if it has already been filled, continuing if either are true. If the variable slow_fill is set to true we use the [yield](https://docs.godotengine.org/en/stable/classes/class_%40gdscript.html#class-gdscript-method-yield) method to pause the for loop. [Yield](https://docs.godotengine.org/en/stable/classes/class_%40gdscript.html#class-gdscript-method-yield) will create a timer lasting 0.07 seconds and connect the 'timeout' signal of the timer to itself. When the timer runs out it will signal 'timeout' and the for loop will unpause.

    		for n in neighbors:
    			var next_tile = current + n
	
	    		if used_cells.has(next_tile) == false:
    				continue		
    			if checked.has(next_tile):
    				continue
    			if get_cellv(next_tile) == fill_color:
    				continue
	    		if slow_fill == true:
	    			yield(get_tree().create_timer(0.07), "timeout")

Now that we know next_tile exists in the TileMap, that it hasn't been checked, and that it hasn't been filled already, we add it to the queue so we can get it's neighbors and we change it's color to the fill color. 

	    		queue.append(next_tile)
	    		set_cellv(next_tile, fill_color)

The for loop then restarts with the next entry in the neighbors array. Once all entries have been checked, the while loop will restart with the next entry in the queue. This will keep going until there are no entries left in the queue.

### Resetting the tiles:

We use a for loop to cycle through all the cells in used_cells and set their index back to what they were at the start. The 'entry' variable is so the entry in default_tilemap_index is the same number as the entry in used_cells.

    func _reset_tiles():
    	var entry = 0

        for u in used_cells:
	        set_cellv(u, default_tilemap_index[entry])
	        entry += 1

### Credits
[Age of Asparagus](https://www.youtube.com/channel/UCkKFLSJjYtKNdFy3P7Q-CAA) for his [flood fill algorithm video](https://www.youtube.com/watch?v=wePC460mmVA).

[Dillinger.io](https://dillinger.io/) which I used to write most of this readme.

[Godot documentation contributors](https://github.com/godotengine/godot-docs/graphs/contributors). I never would've made it this far without them.